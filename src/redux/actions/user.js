export const USER_LOGIN_REQUEST = 'user.login.request';
export const USER_LOGIN_SUCCESS = 'user.login.success';
export const USER_LOGIN_FAILURE = 'user.login.failure';
export const USER_LOGOUT = 'user.logout';

const userLoginSuccessAction = () => ({
  type: USER_LOGIN_SUCCESS
});

const userLoginFailureAction = payload => ({
  type: USER_LOGIN_FAILURE,
  payload
});

const userLoginRequestAction = payload => ({
  type: USER_LOGIN_REQUEST,
  payload
});

const userLogoutAction = () => ({
  type: USER_LOGOUT
});

export { userLoginSuccessAction, userLoginFailureAction, userLogoutAction, userLoginRequestAction };
