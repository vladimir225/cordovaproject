import { createReducer } from 'redux-create-reducer';
import { USER_LOGIN_SUCCESS, USER_LOGIN_FAILURE, USER_LOGOUT } from '../actions/user';

const initState = {
  authorized: false,
  error: null
};

export const user = createReducer(initState, {
  [USER_LOGIN_SUCCESS]: state => ({ ...state, authorized: true }),
  [USER_LOGIN_FAILURE]: (state, { payload }) => ({ ...state, error: payload }),
  [USER_LOGOUT]: state => ({ ...state, authorized: false })
});
