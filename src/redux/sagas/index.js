import { takeEvery, put, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { USER_LOGIN_REQUEST, userLoginSuccessAction, userLoginFailureAction } from '../actions/user';
import Api from '../../Api';

function* loginUser({ payload }) {
  try {
    const data = yield call(Api.loginUser, payload);
    yield put(userLoginSuccessAction(data));
    yield put(push('/'));
  } catch (e) {
    yield put(userLoginFailureAction(e));
  }
}

export default function* loginUserSaga() {
  yield takeEvery(USER_LOGIN_REQUEST, loginUser);
}
