import React from 'react';
import { Switch, Route, Router } from 'react-router';
import LoginForm from '../../components/loginForm';
import HomePage from '../../components/HomePage';
import { Provider } from 'react-redux';
import { store, history } from '../../redux';
import PrivateRouter from '../../components/PrivateRouter';

class App extends React.Component {
  render() {
    return (
      <div>
        <Provider store={store}>
          <Router history={history}>
            <Switch>
              <Route path="/login" component={LoginForm} exact />
              <PrivateRouter>
                <HomePage />
              </PrivateRouter>
            </Switch>
          </Router>
        </Provider>
      </div>
    );
  }
}

export default App;
