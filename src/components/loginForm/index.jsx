import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { userLoginRequestAction } from '../../redux/actions/user';
import style from './loginForm.module.css';

class LoginForm extends React.Component {
  render() {
    const { handleSubmit, dispatch } = this.props;
    return (
      <div className={style.formContainer}>
        <form className={style.form} onSubmit={handleSubmit(val => dispatch(userLoginRequestAction(val)))}>
          <Field className={style.entryField} placeholder="E-mail" name="email" type="email" component="input" />
          <Field
            className={style.entryField}
            placeholder="password"
            name="password"
            type="password"
            component="input"
          />
          <button className={style.loginButton} type="submit">
            log in
          </button>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'LoginForm'
})(LoginForm);
