import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const LoginRouter = ({ component: Component, authorized, ...rest }) => {
  return <Route {...rest} render={props => (authorized ? <Component {...props} /> : <Redirect to="/login" />)} />;
};

const mapStateToProps = ({ user: { authorized } }) => ({
  authorized
});

export default connect(mapStateToProps)(LoginRouter);
