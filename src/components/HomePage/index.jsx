import React from 'react';
import { connect } from 'react-redux';
import { userLogoutAction } from '../../redux/actions/user';

class HomePage extends React.Component {
  render() {
    const { userLogoutAction } = this.props;
    return (
      <div>
        <span>HOME</span>
        <button onClick={userLogoutAction}>log out</button>
      </div>
    );
  }
}

const mapDispatchToProps = { userLogoutAction };

export default connect(null, mapDispatchToProps)(HomePage);
